# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 22:38:03 2019

@author: Eduardo
"""

import pandas as pd
import numpy as np
from sklearn.metrics import  mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn import linear_model
#import matplotlib.pyplot as plt


df_train = pd.read_csv('./train.csv')
df_train = df_train.fillna(0.0)

X = df_train[['NU_NOTA_CN', 'NU_NOTA_CH','NU_NOTA_LC']].copy()

#X['Q001'] = pd.factorize(df_train['Q001'])[0]+1
X['Q002'] = pd.factorize(df_train['Q002'])[0]+1
X['Q006'] = (pd.factorize(df_train['Q006'])[0]+1)*880.0
#X['Q024'] = pd.factorize(df_train['Q024'])[0]+1
#X['Q025'] = pd.factorize(df_train['Q025'])[0]+1
#X['Q026'] = pd.factorize(df_train['Q026'])[0]+1
#X['Q047'] = pd.factorize(df_train['Q047'])[0]+1

Y = df_train['NU_NOTA_MT']
Y = np.ravel(Y)
TP = df_train[['TP_PRESENCA_LC']]

X = X[TP['TP_PRESENCA_LC'] == 1]
Y = Y[TP['TP_PRESENCA_LC'] == 1]

df_test = pd.read_csv('./test.csv')
df_test = df_test.fillna(0.0)
X_test = df_test[['NU_NOTA_CN', 'NU_NOTA_CH','NU_NOTA_LC']].copy()
X_test['Q002'] = pd.factorize(df_test['Q002'])[0]+1
X_test['Q006'] = (pd.factorize(df_test['Q006'])[0]+1)*880.0

X_insc = df_test[['NU_INSCRICAO']]
TP_test = df_test[['TP_PRESENCA_LC']]
X_test_0 = X_test[TP_test['TP_PRESENCA_LC'] == 0]
X_test_1 = X_test[TP_test['TP_PRESENCA_LC'] == 1]
X_test_2 = X_test[TP_test['TP_PRESENCA_LC'] == 2]

X_insc_0 = X_insc[TP_test['TP_PRESENCA_LC'] == 0]
X_insc_1 = X_insc[TP_test['TP_PRESENCA_LC'] == 1]
X_insc_2 = X_insc[TP_test['TP_PRESENCA_LC'] == 2]

a = []

for i in range(100000):
    
    # Splitting data into 80% training and 20% test data:
    X_train, X_test, Y_train, Y_test = train_test_split( X, Y, test_size=0.3, random_state = np.random.randint(100000))

    #regr = linear_model.LinearRegression()
    regr_i = linear_model.ElasticNet(alpha=0.5, l1_ratio=0.5, fit_intercept=True, 
                                   normalize=False, precompute=True, max_iter=1000, 
                                   copy_X=True, tol=0.001, warm_start=False)
    
    regr_i.fit(X_train, Y_train)
    
    #pred = regr.predict(X_test_1)
    pred_i = regr_i.predict(X_test)
   
    #
    #data = pd.DataFrame(pred)
    #result = pd.concat([X_insc_1,data], axis=1, sort=True)
    
    #pred[TP['TP_PRESENCA_LC'] == 0] = 0
    #pred[TP['TP_PRESENCA_LC'] == 2] = 0
    pred_i[pred_i < 50] = 0
    
    #plt.scatter(Y_test, pred)
    #plt.xlabel('Y')
    #plt.ylabel('Pred')
    #plt.title('Relacao Test x pred')
    #plt.show()
    
    # The mean squared error
    #print("Mean squared error: %.2f"
    #      % mean_squared_error(Y_test, pred))
    
    # Explained variance score: 1 is perfect prediction
    #print('Variance score: %.2f' % r2_score(Y_test, pred))
    r2 = r2_score(Y_test, pred_i)
    if i == 0 or (max(a) < r2):
        pred = pred_i
        regr = regr_i
    a.append( r2 )
    
print('Valor max: {}'.format(max(a)))
pred_test = regr.predict(X_test_1)